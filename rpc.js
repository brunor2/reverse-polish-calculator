import {OPERATOR_ERROR, INPUT_ERROR} from './errors'

const OPERATORS = "+-*/"

export const calculate = (input) => {
    input = input.split('')
    let chain = []

    try{
        input.forEach(element => {
            const isNumber = Number.isInteger(parseInt(element))
            if(isNumber){
                chain.push(parseInt(element))
            }else {               
                if(!OPERATORS.includes(element)) throw Error(OPERATOR_ERROR)

                const num1 = chain.pop()
                const num2 = chain.pop()

                if(isNaN(num1) || isNaN(num2)) throw Error(INPUT_ERROR)

                chain.push(eval(num2 + element + num1))
            }
        });
        if(chain.length > 1) throw Error(INPUT_ERROR)
        return chain[0]
    }catch(e){
        return e
    }
}