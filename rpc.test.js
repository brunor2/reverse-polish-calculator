import { expect } from '@jest/globals'
import * as RPC from './rpc'
import {OPERATOR_ERROR, INPUT_ERROR} from './errors'

test("Right input", () => {
    const input = '532-+22*+5/'
    const result = RPC.calculate(input)
    expect(result).toBe(2)
})

test("Wrong input with more operators", () => {
    const input = '532-+22*//+5/'
    const result = RPC.calculate(input)
    expect(result.message).toBe(INPUT_ERROR)
})

test("Wrong input with more numbers", () => {
    const input = '532-+2222*+5/'
    const result = RPC.calculate(input)
    expect(result.message).toBe(INPUT_ERROR)
})

test("Wrong operators", () => {
    const input = '532-+22%+5/'
    const result = RPC.calculate(input)
    expect(result.message).toBe(OPERATOR_ERROR)
})
